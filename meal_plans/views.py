from django.http import Http404
from django.urls import reverse_lazy
from django.views.generic import (
    DetailView,
    CreateView,
    ListView,
    UpdateView,
    DeleteView,
)
from meal_plans.models import MealPlan
from django.contrib.auth.mixins import LoginRequiredMixin


# Create your views here.


class PlansListView(ListView):
    model = MealPlan
    template_name = "plans/list.html"
    paginate_by = 2


class PlansCreateView(
    LoginRequiredMixin, CreateView
):  # shows up in meal_plans/create/
    model = MealPlan
    template_name = "plans/new.html"
    fields = ["name", "date", "recipes"]
    success_url = reverse_lazy("meal_plans")

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)


class PlansDetailView(DetailView):
    model = MealPlan
    template_name = "plans/detail.html"

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class PlansUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlan
    template_name = "plans/edit.html"
    fields = ["name", "date", "recipes"]
    success_url = reverse_lazy("meal_plans")

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class PlansDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlan
    template_name = "plans/delete.html"
    success_url = reverse_lazy("meal_plans")

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)
