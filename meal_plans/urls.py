from django.urls import path

from meal_plans.views import (
    PlansListView,
    PlansCreateView,
    PlansDetailView,
    PlansUpdateView,
    PlansDeleteView,
)


urlpatterns = [
    path("", PlansListView.as_view(), name="meal_plans"),
    path("create/", PlansCreateView.as_view(), name="plans_new"),
    path("<int:pk>/", PlansDetailView.as_view(), name="plans_detail"),
    path("<int:pk>/edit/", PlansUpdateView.as_view(), name="plans_edit"),
    path("<int:pk>/delete/", PlansDeleteView.as_view(), name="plans_delete"),
]
